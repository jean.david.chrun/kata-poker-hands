package poker;

import java.util.List;
import poker.cards.Value;
import poker.rules.PokerRule;

public class RuleResult {

  private final String combinationName;
  private final List<Value> highestValueEnums;
  private final int order;
  private final PokerRule pokerRule;

  public RuleResult(String combinationName, List<Value> highestValueEnum, int order,
      PokerRule pokerRule) {
    this.combinationName = combinationName;
    this.highestValueEnums = highestValueEnum;
    this.order = order;
    this.pokerRule = pokerRule;
  }

  public String getCombinationName() {
    return combinationName;
  }

  public List<Value> getHighestValues() {
    return highestValueEnums;
  }

  public int getOrder() {
    return order;
  }

  public boolean hasHigherRank(RuleResult rule) {
    if (this.order > rule.getOrder()) {
      return true;
    }
    if (this.order == rule.getOrder()) {
      for (int i = 0; i < this.getHighestValues().size(); i++) {
        if (this.highestValueEnums.get(i).getValue() < rule.getHighestValues().get(i).getValue()) {
          return false;
        }
        if (this.highestValueEnums.get(i).getValue() > rule.getHighestValues().get(i).getValue()) {
          return true;
        }
      }
    }
    return false;
  }

  public PokerRule getPokerRule() {
    return pokerRule;
  }


}
