package poker.rules;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import poker.RuleResult;
import poker.cards.Card;
import poker.cards.Value;

public class PairPokerRule implements PokerRule {

  public static String PAIR_POKER_RULE = "pair";
  public static int PAIR_ORDER = 2;

  @Override
  public Optional<RuleResult> compute(List<Card> hand) {
    var values = hand.stream().map(Card::getValue)
        .sorted(Comparator.comparingInt(Value::getValue).reversed())
        .collect(Collectors.toList());
    var extractPairSet = new HashSet<>();
    final var findPairValue = values
        .stream()
        .filter(x -> !extractPairSet.add(x))
        .findFirst();
    if (findPairValue.isPresent()) {
      return Optional.of(buildRuleResult(values, findPairValue.get()));
    }
    return Optional.empty();
  }

  @Override
  public String displayResult(List<Value> hand) {
    return PAIR_POKER_RULE + ": " + hand.get(0);
  }

  private RuleResult buildRuleResult(List<Value> valueEnums, Value pairValueEnum) {
    valueEnums.remove(pairValueEnum);
    valueEnums.remove(pairValueEnum);
    valueEnums.add(0, pairValueEnum);
    return new RuleResult(PAIR_POKER_RULE, valueEnums, PAIR_ORDER, new PairPokerRule());
  }
}
