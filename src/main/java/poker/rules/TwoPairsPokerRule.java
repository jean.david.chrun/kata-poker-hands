package poker.rules;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import poker.RuleResult;
import poker.cards.Card;
import poker.cards.Value;

public class TwoPairsPokerRule implements PokerRule {

  public static String TWO_PAIR_POKER_RULE = "two pairs";
  public static int TWO_PAIRS_ORDER = 3;

  @Override
  public Optional<RuleResult> compute(List<Card> hand) {
    var values = hand.stream()
        .map(Card::getValue)
        .sorted(Comparator.comparingInt(Value::getValue).reversed())
        .collect(Collectors.toList());
    var extractTwoPairSet = new HashSet<>();
    final var findTwoPairsValue = values
        .stream()
        .filter(x -> !extractTwoPairSet.add(x))
        .collect(Collectors.toUnmodifiableList());

    if (findTwoPairsValue.size() == 2) {
      return Optional.of(buildRuleResult(values, findTwoPairsValue));
    }
    return Optional.empty();
  }

  @Override
  public String displayResult(List<Value> hand) {
    return TWO_PAIR_POKER_RULE + ": " + hand.get(0).getValue() + " and " + hand.get(1);
  }

  private RuleResult buildRuleResult(List<Value> values, List<Value> pairValues) {
    values.removeAll(pairValues);
    values.addAll(0, pairValues);
    return new RuleResult(TWO_PAIR_POKER_RULE, values, TWO_PAIRS_ORDER, new TwoPairsPokerRule());
  }

}
