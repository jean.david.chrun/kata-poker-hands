package poker.rules;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import poker.RuleResult;
import poker.cards.Card;
import poker.cards.Value;

public class StraightPokerRule implements PokerRule {

  public static String STRAIGHT_POKER_RULE = "straight";
  public static int STRAIGHT_ORDER = 5;

  @Override
  public Optional<RuleResult> compute(List<Card> hand) {
    final var values = hand.stream().map(Card::getValue)
        .sorted(Comparator.comparingInt(Value::getValue).reversed()).collect(Collectors.toList());
    int endValue = values.get(0).getValue();
    for (int i = 0; i < values.size(); i++) {
      if (values.get(i).getValue() != endValue) {
        return Optional.empty();
      }
      endValue--;
    }
    return Optional
        .of(new RuleResult(STRAIGHT_POKER_RULE, values, STRAIGHT_ORDER, new StraightPokerRule()));
  }

  @Override
  public String displayResult(List<Value> winnerHand) {
    return STRAIGHT_POKER_RULE + ": " + winnerHand.get(0);
  }
}
