package poker.rules;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import poker.RuleResult;
import poker.cards.Card;
import poker.cards.Value;

public class HighCardPokerRule implements PokerRule {

  public static String HIGH_CARD_POKER_RULE = "high card";
  public static int HIGH_CARD_ORDER = 1;

  @Override
  public Optional<RuleResult> compute(List<Card> hand) {
    final var highestCard = hand.stream().map(Card::getValue)
        .sorted(Comparator.comparingInt(Value::getValue).reversed())
        .collect(Collectors.toUnmodifiableList());
    return Optional
        .of(new RuleResult(HIGH_CARD_POKER_RULE, highestCard, HIGH_CARD_ORDER,
            new HighCardPokerRule()));
  }

  @Override
  public String displayResult(List<Value> winnerHand) {
    return HIGH_CARD_POKER_RULE + ": " + winnerHand.get(0);
  }
}
