package poker.rules;

import java.util.List;
import java.util.Optional;
import poker.RuleResult;
import poker.cards.Card;
import poker.cards.Value;

public interface PokerRule {

  Optional<RuleResult> compute(List<Card> hand);

  String displayResult(List<Value> winnerHand);

}
