package poker.rules;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import poker.RuleResult;
import poker.cards.Card;
import poker.cards.Value;

public class ThreeOfAKindPokerRule implements PokerRule {

  public static String THREE_OF_A_KIND_POKER_RULE = "three of a kind";
  public static int THREE_OF_A_KIND_ORDER = 4;

  @Override
  public Optional<RuleResult> compute(List<Card> hand) {
    final var countingMap = hand.stream().map(Card::getValue)
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    final var threeOfAKindValue = countingMap
        .entrySet()
        .stream()
        .filter(x -> x.getValue() == 3L)
        .findFirst();
    return threeOfAKindValue.map(
        entry -> new RuleResult(THREE_OF_A_KIND_POKER_RULE, List.of(entry.getKey()),
            THREE_OF_A_KIND_ORDER,
            new ThreeOfAKindPokerRule()));
  }

  @Override
  public String displayResult(List<Value> hand) {
    return THREE_OF_A_KIND_POKER_RULE + ": " + hand.get(0);
  }
}
