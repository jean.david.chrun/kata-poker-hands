package poker.cards;

public enum Suit {
  CLUBS, DIAMONDS, HEARTS, SPADES
}
