package poker.cards;

import java.util.Objects;

public class Value {

  private final int value;
  private final String code;
  private final String description;

  public Value(int value, String code, String description) {
    this.value = value;
    this.code = code;
    this.description = description;
  }

  public int getValue() {
    return value;
  }

  public String getCode() {
    return code;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Value value1 = (Value) o;
    return value == value1.value && Objects.equals(code, value1.code) && Objects
        .equals(description, value1.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value, code, description);
  }

  @Override
  public String toString() {
    return "Value{" +
        "value=" + value +
        ", code='" + code + '\'' +
        ", description='" + description + '\'' +
        '}';
  }
}
