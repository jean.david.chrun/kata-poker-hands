package poker.cards;

import poker.PokerResult;

public class Card {

  private final Value value;
  private final Suit suit;

  private Card(Value value, Suit suit) {
    this.value = value;
    this.suit = suit;
  }

  public static Card of(String input) {
    return new Card(PokerResult.valueMap.get(input.charAt(0)),
        PokerResult.suitMap.get(input.charAt(1)));
  }

  public Value getValue() {
    return value;
  }

  public Suit getSuit() {
    return suit;
  }

}
