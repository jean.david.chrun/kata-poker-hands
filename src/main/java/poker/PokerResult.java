package poker;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import poker.cards.Card;
import poker.cards.Suit;
import poker.cards.Value;
import poker.rules.HighCardPokerRule;
import poker.rules.PairPokerRule;
import poker.rules.PokerRule;
import poker.rules.ThreeOfAKindPokerRule;
import poker.rules.TwoPairsPokerRule;

public class PokerResult {

  public static String POKER_RESULT_BLACK_WINNER = "Black wins. - with ";
  public static String POKER_RESULT_WHITE_WINNER = "White wins. - with ";
  public static String POKER_RESULT_TIE = "Tie.";

  public static final Map<Character, Value> valueMap = Map.ofEntries(
      Map.entry('2', new Value(2, "2", "2")),
      Map.entry('3', new Value(3, "3", "3")),
      Map.entry('4', new Value(4, "4", "4")),
      Map.entry('5', new Value(5, "5", "5")),
      Map.entry('6', new Value(6, "6", "6")),
      Map.entry('7', new Value(7, "7", "7")),
      Map.entry('8', new Value(8, "8", "8")),
      Map.entry('9', new Value(9, "9", "9")),
      Map.entry('T', new Value(10, "T", "Ten")),
      Map.entry('J', new Value(11, "J", "Jack")),
      Map.entry('Q', new Value(12, "Q", "Queen")),
      Map.entry('K', new Value(13, "K", "King")),
      Map.entry('A', new Value(14, "A", "Ace"))
  );


  public static final Map<Character, Suit> suitMap = Map.ofEntries(
      Map.entry('C', Suit.CLUBS),
      Map.entry('D', Suit.DIAMONDS),
      Map.entry('S', Suit.SPADES),
      Map.entry('H', Suit.HEARTS)
  );

  private final List<PokerRule> rules = List.of(
      new ThreeOfAKindPokerRule(),
      new TwoPairsPokerRule(),
      new PairPokerRule(),
      new HighCardPokerRule()
  );

  public String compare(String input) {
    final var extractInput = input.split("  |:");
    final var whiteHand = convertInputToCards(extractInput[3]);
    final var blackHand = convertInputToCards(extractInput[1]);
    return findBestRuleS(blackHand, whiteHand);
  }

  private List<Card> convertInputToCards(String input) {
    return Arrays.stream(input.trim().split(" ")).map(val ->
        Card.of(val))
        .sorted(Comparator.comparingInt((Card x) -> x.getValue().getValue()).reversed())
        .collect(Collectors.toUnmodifiableList());
  }

  public String findBestRuleS(List<Card> blackHand, List<Card> whiteHand) {
    final var blackRule = applyPokerRules(blackHand);
    final var whiteRule = applyPokerRules(whiteHand);
    return findResult(blackRule, whiteRule);
  }

  public RuleResult applyPokerRules(List<Card> hand) {
    for (PokerRule rule : rules) {
      var result = rule.compute(hand);
      if (result.isPresent()) {
        return result.get();
      }
    }
    throw new IllegalStateException();
  }

  private String findResult(RuleResult blackRule, RuleResult whiteRule) {
    if (blackRule.hasHigherRank(whiteRule)) {
      return buildWinnerResult(POKER_RESULT_BLACK_WINNER, blackRule, whiteRule);
    }
    if (whiteRule.hasHigherRank(blackRule)) {
      return buildWinnerResult(POKER_RESULT_WHITE_WINNER, whiteRule, blackRule);
    }
    return POKER_RESULT_TIE;
  }

  private String buildWinnerResult(String winner, RuleResult winnerRuleResult,
      RuleResult loserRuleResult) {
    if (winnerRuleResult.getOrder() != loserRuleResult.getOrder()) {
      return winner + winnerRuleResult.getPokerRule()
          .displayResult(winnerRuleResult.getHighestValues());
    }
    return winner + buildWinnerResultWithDifferentRule(winnerRuleResult, loserRuleResult);
  }

  private String buildWinnerResultWithDifferentRule(RuleResult winnerRuleResult,
      RuleResult loserRuleResult) {
    var output = winnerRuleResult.getCombinationName() + ": ";
    for (int i = 0; i < winnerRuleResult.getHighestValues().size(); i++) {
      if (winnerRuleResult.getHighestValues().get(i).getValue() > loserRuleResult.getHighestValues()
          .get(i).getValue()) {
        return output + winnerRuleResult.getHighestValues().get(i).getDescription();
      }
    }
    return output;
  }
}
