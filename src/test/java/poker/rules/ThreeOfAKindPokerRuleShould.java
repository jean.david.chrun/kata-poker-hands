package poker.rules;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import poker.PokerResult;
import poker.cards.Card;


@DisplayName("Three of a kind poker rule")
class ThreeOfAKindPokerRuleShould {

  @Test
  @DisplayName("should display Rule result with Three-Of-A-Kind card")
  public void findThreeOfAKindCardOnHand() {
    final var hand = Arrays.asList(
        Card.of("4H"),
        Card.of("4H"),
        Card.of("4H"),
        Card.of("QH"),
        Card.of("TH"));
    final var expectedThreeOfAKind = Arrays
        .asList(PokerResult.valueMap.get('4'));

    final var actualRuleResult = new ThreeOfAKindPokerRule().compute(hand).get();

    assertEquals(ThreeOfAKindPokerRule.THREE_OF_A_KIND_POKER_RULE,
        actualRuleResult.getCombinationName());
    assertEquals(expectedThreeOfAKind, actualRuleResult.getHighestValues());
    assertEquals(ThreeOfAKindPokerRule.THREE_OF_A_KIND_ORDER, actualRuleResult.getOrder());
  }

  @Test
  @DisplayName("should return empty Rule result")
  public void notFindThreeOfAKindCardResult() {
    final var hand = Arrays.asList(
        Card.of("4H"),
        Card.of("4H"),
        Card.of("KH"),
        Card.of("QH"),
        Card.of("TH"));

    final var actualRuleResult = new ThreeOfAKindPokerRule().compute(hand);

    assertTrue(actualRuleResult.isEmpty());
  }

}