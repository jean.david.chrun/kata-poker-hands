package poker.rules;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import poker.PokerResult;
import poker.cards.Card;

@DisplayName("Pair poker rule")
class PairPokerRuleShould {

  @Test
  @DisplayName("should display Rule result with one pair card")
  public void findPairCardOnHand() {
    final var hand = Arrays.asList(
        Card.of("4H"),
        Card.of("4H"),
        Card.of("KH"),
        Card.of("QH"),
        Card.of("TH"));
    final var expectedPairValues = Arrays
        .asList(PokerResult.valueMap.get('4'), PokerResult.valueMap.get('K'),
            PokerResult.valueMap.get('Q'), PokerResult.valueMap.get('T'));

    final var actualRuleResult = new PairPokerRule().compute(hand).get();

    assertEquals(PairPokerRule.PAIR_POKER_RULE, actualRuleResult.getCombinationName());
    assertEquals(expectedPairValues, actualRuleResult.getHighestValues());
    assertEquals(PairPokerRule.PAIR_ORDER, actualRuleResult.getOrder());
  }

  @Test
  @DisplayName("should return empty rule result")
  public void notFindPairCardResult() {
    final var hand = Arrays.asList(
        Card.of("4H"),
        Card.of("8H"),
        Card.of("KH"),
        Card.of("QH"),
        Card.of("TH"));

    final var actualRuleResult = new PairPokerRule().compute(hand);

    assertTrue(actualRuleResult.isEmpty());
  }

}