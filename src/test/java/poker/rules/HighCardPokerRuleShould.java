package poker.rules;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import poker.PokerResult;
import poker.cards.Card;

@DisplayName("High Card Poker Rule")
class HighCardPokerRuleShould {

  @Test
  @DisplayName("should display Rule result with the Highest card")
  public void findHighCardOnHand() {
    final var hand = Arrays.asList(
        Card.of("4D"),
        Card.of("6H"),
        Card.of("AH"),
        Card.of("QH"),
        Card.of("TH"));
    final var expectedHighestValues = Arrays
        .asList(PokerResult.valueMap.get('A'), PokerResult.valueMap.get('Q'),
            PokerResult.valueMap.get('T'), PokerResult.valueMap.get('6'),
            PokerResult.valueMap.get('4'));

    final var actualRuleResult = new HighCardPokerRule().compute(hand).get();

    assertEquals(HighCardPokerRule.HIGH_CARD_POKER_RULE, actualRuleResult.getCombinationName());
    assertEquals(expectedHighestValues, actualRuleResult.getHighestValues());
    assertEquals(HighCardPokerRule.HIGH_CARD_ORDER, actualRuleResult.getOrder());
  }

}