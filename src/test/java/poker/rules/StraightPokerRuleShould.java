package poker.rules;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import poker.PokerResult;
import poker.cards.Card;

class StraightPokerRuleShould {

  @Test
  public void findStraightCardOnHand() {
    final var hand = Arrays.asList(
        Card.of("9H"),
        Card.of("JH"),
        Card.of("KH"),
        Card.of("QH"),
        Card.of("TH"));
    final var expectedStraightValues = Arrays
        .asList(PokerResult.valueMap.get('K'), PokerResult.valueMap.get('Q'),
            PokerResult.valueMap.get('J'), PokerResult.valueMap.get('T'),
            PokerResult.valueMap.get('9'));

    final var actualRuleResult = new StraightPokerRule().compute(hand).get();

    assertEquals(StraightPokerRule.STRAIGHT_POKER_RULE, actualRuleResult.getCombinationName());
    assertEquals(expectedStraightValues, actualRuleResult.getHighestValues());
    assertEquals(StraightPokerRule.STRAIGHT_ORDER, actualRuleResult.getOrder());
  }

  @Test
  @DisplayName("should return empty Rule result")
  public void notFindThreeOfAKindCardResult() {
    final var hand = Arrays.asList(
        Card.of("4H"),
        Card.of("4H"),
        Card.of("KH"),
        Card.of("QH"),
        Card.of("TH"));

    final var actualRuleResult = new StraightPokerRule().compute(hand);

    assertTrue(actualRuleResult.isEmpty());
  }

}