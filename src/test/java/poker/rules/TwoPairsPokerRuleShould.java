package poker.rules;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import poker.PokerResult;
import poker.cards.Card;

@DisplayName("Two pairs poker rule")
class TwoPairsPokerRuleShould {

  @Test
  @DisplayName("should display Rule result with two pairs card")
  public void findTwoPairsCardOnHand() {
    final var hand = Arrays.asList(
        Card.of("4H"),
        Card.of("4H"),
        Card.of("KH"),
        Card.of("KH"),
        Card.of("TH"));
    final var expectedTwoPairsValues = Arrays
        .asList(PokerResult.valueMap.get('K'), PokerResult.valueMap.get('4'),
            PokerResult.valueMap.get('T'));

    final var actualRuleResult = new TwoPairsPokerRule().compute(hand).get();

    assertEquals(TwoPairsPokerRule.TWO_PAIR_POKER_RULE, actualRuleResult.getCombinationName());
    assertEquals(expectedTwoPairsValues, actualRuleResult.getHighestValues());
    assertEquals(TwoPairsPokerRule.TWO_PAIRS_ORDER, actualRuleResult.getOrder());
  }

  @Test
  @DisplayName("should return empty rule result")
  public void notFindTwoPairCardResult() {
    final var hand = Arrays.asList(
        Card.of("4H"),
        Card.of("4H"),
        Card.of("KH"),
        Card.of("QH"),
        Card.of("TH"));

    final var actualRuleResult = new TwoPairsPokerRule().compute(hand);

    assertTrue(actualRuleResult.isEmpty());
  }

}