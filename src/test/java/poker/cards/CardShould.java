package poker.cards;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import poker.PokerResult;

class CardShould {

  @Test
  public void createOneCard() {
    final var cardInput = "AD";
    final var expectedValue = PokerResult.valueMap.get('A');
    final var expectedSuit = PokerResult.suitMap.get('D');

    final var card = Card.of(cardInput);

    assertEquals(expectedValue, card.getValue());
    assertEquals(expectedSuit, card.getSuit());
  }

}