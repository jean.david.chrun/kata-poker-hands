package poker;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import poker.cards.Card;
import poker.rules.ThreeOfAKindPokerRule;

@DisplayName("Poker Result")
@ExtendWith(MockitoExtension.class)
class PokerResultShould {

  @Mock
  private PrintStream printStream;
  private PokerResult pokerResult;

  @BeforeEach
  void setup() {
    this.pokerResult = new PokerResult();
  }

  @Test
  @DisplayName("should find the best rule of a hand")
  public void findBestRuleOfHand() {
    final var hand = Arrays.asList(
        Card.of("4H"),
        Card.of("4H"),
        Card.of("4H"),
        Card.of("QH"),
        Card.of("TH"));
    final var expectedValues = List.of(PokerResult.valueMap.get('4'));

    final var actualRule = pokerResult.applyPokerRules(hand);

    assertEquals(ThreeOfAKindPokerRule.THREE_OF_A_KIND_POKER_RULE, actualRule.getCombinationName());
    assertEquals(expectedValues, actualRule.getHighestValues());
    assertEquals(ThreeOfAKindPokerRule.THREE_OF_A_KIND_ORDER, actualRule.getOrder());
  }

  @Test
  @DisplayName("should display Tie")
  public void displayTieResult() {
    final var black = Arrays.asList(
        Card.of("4H"),
        Card.of("4H"),
        Card.of("KH"),
        Card.of("QH"),
        Card.of("TH"));
    final var white = List.copyOf(black);
    final var expectedTieMessage = PokerResult.POKER_RESULT_TIE;

    final var actualMessage = pokerResult.findBestRuleS(black, white);

    assertEquals(expectedTieMessage, actualMessage);
  }

  @ParameterizedTest
  @CsvSource({
      "'Black: 2H 3D 5S 9C KD  White: 2C 3H 4S 8C AH','White wins. - with high card: Ace'",
      "'Black: 2H 4S 4C 2D 4H  White: 2S 8S AS QS 3S','Black wins. - with full house: 4 over 2'",
      "'Black: 2H 3D 5S 9C KD  White: 2C 3H 4S 8C KH','Black wins. - with high card: 9'",
      "'Black: 2H 3D 5S 9C KD  White: 2D 3H 5C 9S KH','Tie.'"
  })
  void beAbleToCompare(String input, String expected) {
    assertEquals(expected, new PokerResult().compare(input));
  }

}